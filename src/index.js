const {SimpleTest} = require("./SimpleTest");
const { nerdvision } = require('@nerdvision/agent')

nerdvision.start()

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

async function main() {
    let ts = new SimpleTest("This is a test");
    for (; ;) {
        try {
            ts.message(ts.newId());
        } catch (e) {
            await nerdvision.captureException(e)
            console.log(e)
            ts.reset();
        }

        await sleep(100);
    }
}

main();
