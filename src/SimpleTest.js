const {BaseTest} = require("./BaseTest");


class SimpleTest extends BaseTest {

    startedAt = new Date().valueOf();
    testName;
    maxExecutions;
    cnt = 0;
    charCounter = {};

    constructor(testName) {
        super();
        this.testName = testName
        this.maxExecutions = this.nextMax();
    }

    message(
        uuid
    ) {
        console.log(this.cnt + ":" + uuid);
        this.cnt += 1;

        this.checkEnd(this.cnt, this.maxExecutions);

        const info = this.makeCharCountMap(uuid);
        this.merge(this.charCounter, info);
        if ((this.cnt % 30) === 0) {
            this.dump();
        }
    }

    merge(charCounter, newInfo) {

        Object.keys(newInfo).forEach(key => {
            const newVal = newInfo[key]
            const curVal = charCounter[key]
            if (!curVal) {
                charCounter[key] = newVal
            } else {
                charCounter[key] = newVal + curVal
            }
        })
    }

    dump() {
        console.log(this.charCounter)
        this.charCounter = {}
    }

    checkEnd(val, max) {
        if (val > max) {
            throw new Error("Hit max executions " + val + " " + max);
        }
    }

    toString() {
        return this.constructor.name + ":" + this.testName + ":" + this.startedAt;
    }

    reset() {
        this.cnt = 0;
        this.maxExecutions = this.nextMax();
    }
}

module.exports = {
    SimpleTest
}
