const {v4: uuidv4} = require('uuid');

class BaseTest {

    newId() {
        return uuidv4()
    }


    nextMax() {
        return Math.floor(Math.random() * 100);
    }


    makeCharCountMap(str) {
        let res = {}

        for (let i = 0; i < str.length; i++) {
            let c = str.charAt(i);
            let cnt = res[c];
            if (cnt == null) {
                res[c] = 0;
            } else {
                res[c] = cnt + 1;
            }
        }

        return res;
    }

}

module.exports = {
    BaseTest
}
