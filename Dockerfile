FROM node:14
MAINTAINER Ben Donnelly <ben_donnelly@intergral.com>

ENV NV_NAME=javascript-test-app
ENV NV_REPO_URL=https://gitlab.com/nerd-vision/test-apps/node/javascript

# Add our service
ADD . /app
WORKDIR /app

ENTRYPOINT ["npm", "run", "start"]
